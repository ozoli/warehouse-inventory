package main

import (
	"fmt"
	"github.com/gorilla/mux"
	warehouseinventory "gitlab.com/warehouse-inventory/internal/app/warehouse-inventory"
	"gitlab.com/warehouse-inventory/internal/pkg/config"
	inventoryapi "gitlab.com/warehouse-inventory/internal/pkg/inventory-api"
	inventorystore "gitlab.com/warehouse-inventory/internal/pkg/inventory-store"
	"log"
	"net/http"
	"time"
)

// localPort to be externalised with config TODO
var localPort = 9090

// init() loads warehouse-inventory config
func init() {
	dbConfig, err := config.LoadDatabaseConfig()
	if err != nil {
		log.Fatalf("Error loading database config: %+v", err)
	}
	log.Printf("Using database config: %+v", dbConfig)
	if err = inventorystore.SetupInventoryRepository(dbConfig); err != nil {
		log.Fatalf("Error setting up Inventory database: %+v", err)
	}
}

func main() {
	closeApplication := func() error {
		if err := inventorystore.InventoryStore.Close(); err != nil {
			log.Printf("Error closing inventory store: %v", err)
			return err
		}
		log.Println("Close Databases to free up resources or connections")
		return nil
	}

	// Create Server and Route Handlers
	r := mux.NewRouter()
	//r.Use(logging_common.LoggingMiddleware)

	r.HandleFunc("/", warehouseinventory.StatusHandler).Methods("GET")
	//r.Handle("/metrics", promhttp.Handler()).Methods("GET")
	r.HandleFunc("/inventory", inventoryapi.CreateInventoryHandler).Methods("POST")

	r.HandleFunc("/products", inventoryapi.GetProductsHandler).Methods("GET")
	r.HandleFunc("/products", inventoryapi.CreateProductsHandler).Methods("POST")
	r.HandleFunc("/products", inventoryapi.RemoveProductHandler).Methods("DELETE")

	srv := &http.Server{
		Handler:      r,
		Addr:         fmt.Sprintf(":%d", localPort),
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	go func() {
		log.Printf("Starting warehouse inventory serveron port: %d", localPort)
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()

	warehouseinventory.WaitForShutdown(srv, "warehouse-inventory", closeApplication)
}
