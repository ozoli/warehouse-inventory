# Warehouse Inventory

GoLang implementation of a warehouse inventory solution. 

## Prerequisites 

To install Go please follow the instructions [here](https://golang.org/doc/install).

## Install

To get start clone the Git repo and install the Go dependencies

```bash
git clone https://gitlab.com/ozoli/warehouse-inventory
cd warehouse-inventory
go mod vendor
```

## Develop

To run the unit tests:
```
make test
```

There are also make targets for lint (Code style) and race for race detector see [here](https://blog.golang.org/race-detector)

## Running

To run the webserver run (port 9090 needs to be open):

```bash
go run cmd/warehouse-inventory/main.go
```

If you use Postman there is a localhost collection available [here](https://www.getpostman.com/collections/5849a88a0183d17ab296)

To load files the following curl commands can be used:

### Load Inventory
```bash
curl -H "Content-Type: application/json" -d "@test/inventory.json" http://localhost:9090/inventory
```

### Load Products
```bash
curl -H "Content-Type: application/json" -d "@test/products.json" http://localhost:9090/products
```

### Sell (Remove) Product
```bash
curl -X DELETE -H "Content-Type: application/json" -d "@test/product.json" http://localhost:9090/products
```

## Future Work 

- GitLab CI pipelines
- Implement SQL implementation of Inventory Store
- Add BDD tests using Cucumber
- Add two stage Docker build
- Add Docker Compose with SQL database (eg MySQL or Postgres)
- Docker tests
- Add logging wrapper and log to Fluentd for ElasticSearch
- Add metrics endpoint for Prometheus

## License
Copyright 2020 Oliver Carr

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
