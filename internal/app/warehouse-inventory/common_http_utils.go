package warehouseinventory

import (
	"encoding/json"
	"net/http"
)

// StatusHandler godoc
// @Summary Status handler
// @Description empty status endpoint needed for GCP ingress
// @Produce  json
// @Success 200 {object} string
// @Router / [get]
func StatusHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string]string{"status": "OK"})
}
