package warehouseinventory

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// WaitForShutdown setup channel to wait for signal to shutdown
func WaitForShutdown(srv *http.Server, appName string, closeApplication func() error) {
	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// Block until we receive our signal.
	<-interruptChan

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 10)
	defer cancel()
	srv.Shutdown(ctx)

	log.Printf("Closing application: %s", appName)
	if err := closeApplication(); err != nil {
		log.Printf("WARN Error closing application: %v", err)
	}
	log.Printf("Shutting down: %s", appName)
	os.Exit(0)
}

