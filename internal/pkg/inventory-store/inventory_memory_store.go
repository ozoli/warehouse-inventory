package inventorystore

import (
	"fmt"
	"gitlab.com/warehouse-inventory/internal/pkg/products"
	"log"
	"sort"
	"sync"
)

// Ensure memoryInventoryStore conforms to the InventoryRepository interface.
var _ InventoryRepository = &memoryInventoryStore{}

// memoryInventoryStore is a simple in-memory persistence layer for products and inventory entities.
type memoryInventoryStore struct {
	mu       sync.Mutex
	stockItems map[string]*products.StockItem // maps from Article ID UUID to StockItem
	products   map[string]*products.Product   // maps from Product UUID to StockItem
}

func newMemoryDB() *memoryInventoryStore {
	return &memoryInventoryStore{
		products:   make(map[string]*products.Product),
		stockItems: make(map[string]*products.StockItem),
	}
}

func (m memoryInventoryStore) CreateStockItem(stockItem *products.StockItem) error {
	log.Printf("CreateStockItem: %+v", stockItem)
	if stockItem == nil {
		return fmt.Errorf("CreateStockItem stock item nil")
	} else if stockItem.ArticleID == "" {
		return fmt.Errorf("CreateStockItem stock item article ID is empty")
	}
	m.mu.Lock()
	defer m.mu.Unlock()
	// TODO: Warn if StockItem already exists
	m.stockItems[stockItem.ArticleID] = stockItem
	return nil
}

func (m memoryInventoryStore) CreateProduct(product *products.Product) error {
	log.Printf("CreateProduct: %+v", product)
	if product == nil {
		return fmt.Errorf("CreateProduct product nil")
	} else if product.Name == "" || product.UUID == "" {
		return fmt.Errorf("CreateProduct name or UUID is empty: %+v", product)
	} else if len(product.ContainArticles) == 0 {
		return fmt.Errorf("CreateProduct no articles in product: %+v", product)
	}
	m.mu.Lock()
	defer m.mu.Unlock()

	// TODO Validate all article are present in the inventory and inventory has enough stock
	m.products[product.UUID] = product
	return nil
}

// TODO move to controller??
// GetProductsWithQuantity loop through all products using a copy of the inventory remove
func (m memoryInventoryStore) GetProductsWithQuantity() ([]*products.ProductQuantity, error) {
	m.mu.Lock()
	defer m.mu.Unlock()

	productQuantities := make([]*products.ProductQuantity, 0)
	for _, product := range m.products {
		articleIds := getArticleIdsFromProduct(product)
		stockItems, err := m.getInventoryForArticleIds(articleIds)
		if err != nil {
			return nil, fmt.Errorf("error finding stocks for articleIds: %v %v", articleIds, err)
		}
		if len(articleIds) != len(stockItems) {
			return nil, fmt.Errorf("articles and stock items different lengths: %+v %+v", articleIds, stockItems)
		}
		// Algo. find max mod of articleAmount and stock
	    productQuantities = append(productQuantities, &products.ProductQuantity{
			Product:  cloneProduct(product),
			Quantity: maxAmountPossible(articleIds, stockItems),
		})
	}
	return productQuantities, nil
}

func cloneProduct(product *products.Product) products.Product {
	containsArticlesCopy := make([]products.ContainArticles, len(product.ContainArticles))
	copy(containsArticlesCopy, product.ContainArticles)
	return products.Product{
		Name:            product.Name,
		UUID:            product.UUID,
		ContainArticles: containsArticlesCopy,
	}
}

func maxAmountPossible(articles []products.ContainArticles, stockItems []*products.StockItem) int {
	amountsPossible := make([]int, 0)
	for idx := 0; idx < len(articles); idx++ {
		amountPossible := stockItems[idx].StockInt / articles[idx].AmountOfInt // rounds down to nearest integer
		if amountPossible == 0 {
			log.Printf("Stock ran out: %+v %+v", articles, stockItems)
			return 0
		}
		amountsPossible = append(amountsPossible, amountPossible)
	}
	log.Printf("maxAmountPossible pre sort: %+v", amountsPossible)
	sort.Ints(amountsPossible)
	log.Printf("maxAmountPossible post sort: %+v", amountsPossible)
	return amountsPossible[0]
}

func (m memoryInventoryStore) getInventoryForArticleIds(articles []products.ContainArticles) ([]*products.StockItem, error) {
	stockItems := make([]*products.StockItem, 0)
	for _, article := range articles {
		stockItem, ok := m.stockItems[article.ArtID]
		if !ok {
			return nil, fmt.Errorf("StockItem not found for article: %+v", article)
		}
		stockItems = append(stockItems, &products.StockItem{
			ArticleID: stockItem.ArticleID,
			Name:      stockItem.Name,
			Stock:     stockItem.Stock,
			StockInt:  stockItem.StockInt,
		})
	}
	return stockItems, nil
}

func getArticleIdsFromProduct(product *products.Product) []products.ContainArticles {
	articles := make([]products.ContainArticles, 0)
	for _, containArticle := range product.ContainArticles {
		articles = append(articles, products.ContainArticles{
			ArtID:       containArticle.ArtID,
			ArtIDInt:    containArticle.ArtIDInt,
			AmountOf:    containArticle.AmountOf,
			AmountOfInt: containArticle.AmountOfInt,
		})
	}
	return articles
}

func (m memoryInventoryStore) RemoveProduct(product *products.Product) error {
	if product == nil {
		return fmt.Errorf("RemoveProduct nil product")
	}
	m.mu.Lock()
	defer m.mu.Unlock()
	for _, article := range product.ContainArticles {
		stockItem, ok := m.stockItems[article.ArtID]
		if !ok {
			return fmt.Errorf("could not find %+v to remove", article)
		}
		newStockLevel := stockItem.StockInt - 1
		if newStockLevel < 0 {
			newStockLevel = 0
		}
		stockItem.StockInt = newStockLevel
		stockItem.Stock = fmt.Sprintf("%d", newStockLevel)
		m.stockItems[article.ArtID] = stockItem
	}
	log.Printf("RemoveProduct completed: %+v", product)
	return nil
}

func (m memoryInventoryStore) Close() error {
	m.mu.Lock()
	defer m.mu.Unlock()
	m.stockItems = nil
	return nil
}
