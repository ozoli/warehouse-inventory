package inventorystore

import "gitlab.com/warehouse-inventory/internal/pkg/products"

// InventoryRepository provides the Create, Fetch, List and Delete operations on Accounts
type InventoryRepository interface {

	// Create a StockItem using the StockItem given
	CreateStockItem(stockItem *products.StockItem) error

	// Create a Product using the Product given and return the Product created
	CreateProduct(product *products.Product) error

	// Get all ProductWithQuantity from the Inventory
	GetProductsWithQuantity() ([]*products.ProductQuantity, error)

	// Remove product from Inventory
	RemoveProduct(product *products.Product) error

	// Close closes the inventory, freeing up any used resources.
	Close() error
}
