package inventorystore

import (
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/warehouse-inventory/internal/pkg/products"
	"log"
	"strconv"
)

// LoadProducts into the InventoryRepository using the given Products
func LoadProducts(productsToLoad *products.LoadProducts) error {
	log.Printf("LoadProducts: %+v", productsToLoad)
	for _, product := range productsToLoad.Products {
		uuid, err := uuid.NewUUID()
		if err != nil {
			return fmt.Errorf("error generating UUID for %+v", product)
		}
		product.UUID = uuid.String()
		enrichedContainArticles := make([]products.ContainArticles, 0)
		for _, containArticle := range product.ContainArticles {
			artIDInt, err := strconv.Atoi(containArticle.ArtID)
			if err != nil {
				return fmt.Errorf("error parsing ArtId %+v for %+v", containArticle, product)
			}
			containArticle.ArtIDInt = artIDInt

			amountOfInt, err := strconv.Atoi(containArticle.AmountOf)
			if err != nil {
				return fmt.Errorf("error parsing AmountOfIn %+v for %+v", containArticle, product)
			}
			containArticle.AmountOfInt = amountOfInt
			enrichedContainArticles = append(enrichedContainArticles, containArticle)
		}
		if err := InventoryStore.CreateProduct(&products.Product{
			Name:            product.Name,
			UUID:            product.UUID,
			ContainArticles: enrichedContainArticles,
		}); err != nil {
			return fmt.Errorf("error loading product: %+v", product)
		}
	}
	log.Printf("LoadedProducts: %+v", productsToLoad)
	return nil
}

// LoadStockItems into the InventoryRepository using the given Products
func LoadStockItems(inventory *products.Inventory) error {
	log.Printf("LoadStockItems: %+v", inventory)
	for _, stockItem := range inventory.StockItems {
		stockInt, err := strconv.Atoi(stockItem.Stock)
		if err != nil {
			return fmt.Errorf("stock not an int: %+v %v", stockItem, err)
		}
		stockItem.StockInt = stockInt
		if err := InventoryStore.CreateStockItem(&products.StockItem{
			ArticleID: stockItem.ArticleID,
			Name:      stockItem.Name,
			Stock:     stockItem.Stock,
			StockInt:  stockItem.StockInt,
		}); err != nil {
			return fmt.Errorf("error loading stock item: %+v", stockItem)
		}
	}
	log.Printf("Loaded SockItems: %+v", inventory)
	return nil
}

// GetProductsQuantities the products and number available out of the Inventory
func GetProductsQuantities() ([]*products.ProductQuantity, error) {
	log.Println("GetProductsQuantities")
	productsQuantities, err := InventoryStore.GetProductsWithQuantity()
	if err != nil {
		return nil, fmt.Errorf("error finding products quantities: %v", err)
	}
	log.Printf("GetProductsQuantities: %+v", productsQuantities)
	return productsQuantities, err
}

// RemoveProduct remove a product from the Inventory
func RemoveProduct(product *products.Product) error {
	log.Printf("RemoveProduct: %+v", product)
	return InventoryStore.RemoveProduct(product)
}
