package inventorystore

import (
	"fmt"
	"gitlab.com/warehouse-inventory/internal/pkg/config"
)

// InventoryStore is used to access inventory and products
var InventoryStore InventoryRepository

// SetupInventoryRepository given the Database Configuration setup the InventoryStore
func SetupInventoryRepository(config *config.DatabaseConfiguration) error {
	if config.StorageType != "memory" {
		return fmt.Errorf("InventoryRepository only supports memory storage")
	}
	InventoryStore = newMemoryDB()
	return nil
}
