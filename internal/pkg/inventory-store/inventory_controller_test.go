package inventorystore

import (
	"encoding/json"
	"gitlab.com/warehouse-inventory/internal/pkg/config"
	"gitlab.com/warehouse-inventory/internal/pkg/products"
	"testing"
)

var exampleInventoryJSON = `
{
  "inventory": [
    {
      "art_id": "1",
      "nam": "leg",
      "stock": "12"
    },
    {
      "art_id": "2",
      "name": "screw",
      "stock": "17"
    },
    {
      "art_id": "3",
      "name": "seat",
      "stock": "2"
    },
    {
      "art_id": "4",
      "name": "table top",
      "stock": "1"
    }
  ]
}`

var exampleProductsJSON = `
{
  "products": [
    {
      "name": "Dining Chair",
      "contain_articles": [
        {
          "art_id": "1",
          "amount_of": "4"
        },
        {
          "art_id": "2",
          "amount_of": "8"
        },
        {
          "art_id": "3",
          "amount_of": "1"
        }
      ]
    },
    {
      "name": "Dinning Table",
      "contain_articles": [
        {
          "art_id": "1",
          "amount_of": "4"
        },
        {
          "art_id": "2",
          "amount_of": "8"
        },
        {
          "art_id": "4",
          "amount_of": "1"
        }
      ]
    }
  ]
}`

func TestLoadProducts(t *testing.T) {
	if err := SetupInventoryRepository(&config.DatabaseConfiguration{StorageType: "memory"}); err != nil {
		t.Errorf("error setting up inventory repository")
	}

	var loadProducts products.LoadProducts
	if err := json.Unmarshal([]byte(exampleProductsJSON), &loadProducts); err != nil {
		t.Errorf("error parsing JSON: %s %v", exampleProductsJSON, err)
	}

	if len(loadProducts.Products) == 0 {
		t.Errorf("No products loaded from JSON: %s", exampleProductsJSON)
	}

	if err := LoadProducts(&loadProducts); err != nil {
		t.Errorf("error loading loadProducts: %v", err)
	}
}

func TestLoadStockItems(t *testing.T) {
	if err := SetupInventoryRepository(&config.DatabaseConfiguration{StorageType: "memory"}); err != nil {
		t.Errorf("error setting up inventory repository")
	}

	var inventory products.Inventory
	if err := json.Unmarshal([]byte(exampleInventoryJSON), &inventory); err != nil {
		t.Errorf("error parsing JSON: %s %v", exampleInventoryJSON, err)
	}

	if len(inventory.StockItems) == 0 {
		t.Errorf("No stock items loaded from JSON: %s", exampleInventoryJSON)
	}

	if err := LoadStockItems(&inventory); err != nil {
		t.Errorf("error loading products: %v", err)
	}
}

func TestGetProductsQuantities(t *testing.T) {
	if err := SetupInventoryRepository(&config.DatabaseConfiguration{StorageType: "memory"}); err != nil {
		t.Errorf("error setting up inventory repository")
	}

	var inventory products.Inventory
	if err := json.Unmarshal([]byte(exampleInventoryJSON), &inventory); err != nil {
		t.Errorf("error parsing JSON: %s %v", exampleInventoryJSON, err)
	}

	if err := LoadStockItems(&inventory); err != nil {
		t.Errorf("error loading products: %v", err)
	}

	var loadProducts products.LoadProducts
	if err := json.Unmarshal([]byte(exampleProductsJSON), &loadProducts); err != nil {
		t.Errorf("error parsing JSON: %s %v", exampleProductsJSON, err)
	}

	if len(loadProducts.Products) == 0 {
		t.Errorf("No products loaded from JSON: %s", exampleProductsJSON)
	}

	if err := LoadProducts(&loadProducts); err != nil {
		t.Errorf("error loading loadProducts: %v", err)
	}

	productsQuantities, err := GetProductsQuantities()
	if err != nil {
		t.Errorf("error finding products quantities: %v", err)
	}

	if len(productsQuantities) != len(loadProducts.Products) {
		t.Errorf("error differnt length load products and products quantities: %+v %+v", productsQuantities, loadProducts)
	}

	for _, productQuantity := range productsQuantities {
		if productQuantity.Quantity == 0 {
			t.Errorf("Error populating Product Quantities: %+v", productsQuantities)
		}
		if productQuantity.Product.Name == "Dinning Table" && productQuantity.Quantity != 1 {
			t.Errorf("Expected 1 Dinning Table: %+v", productsQuantities)
		}
		if productQuantity.Product.Name == "Dining Chair" && productQuantity.Quantity != 2 {
			t.Errorf("Expected 2 Dining Chairs: %+v", productsQuantities)
		}
	}

	// Remove one Dining Chair
	for _, productQuantity := range productsQuantities {
		if productQuantity.Product.Name == "Dining Chair" {
			if err = RemoveProduct(&productQuantity.Product); err != nil {
				t.Errorf("error removing product: %+v %v", productQuantity, err)
			}
		}
	}

	productsQuantitiesMinusOneProduct, err := GetProductsQuantities()
	if err != nil {
		t.Errorf("error finding products quantities: %v", err)
	}

	for _, productQuantity := range productsQuantitiesMinusOneProduct {
		if productQuantity.Quantity == 0 {
			t.Errorf("Error populating Product Quantities: %+v", productsQuantities)
		}
		if productQuantity.Product.Name == "Dinning Table" && productQuantity.Quantity != 1 {
			t.Errorf("Expected 1 Dinning Table: %+v", productsQuantities)
		}
		if productQuantity.Product.Name == "Dining Chair" && productQuantity.Quantity != 1 {
			t.Errorf("Expected 2 Dining Chairs: %+v", productsQuantities)
		}
	}
}
