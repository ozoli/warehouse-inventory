package inventoryapi

import (
	"encoding/json"
	"fmt"
	inventory_store "gitlab.com/warehouse-inventory/internal/pkg/inventory-store"
	"gitlab.com/warehouse-inventory/internal/pkg/products"
	"io/ioutil"
	"log"
	"net/http"
)

// CreateInventoryHandler godoc
// @Summary Create inventory
// @Description create inventory given in the JSON body
// @Accept json
// @Produce json
// @Param products body products.Inventory true "Inventory to add"
// @Success 200 {object} string
// @Failure 500 {object} string "database error creating inventory"
// @Router /inventory/ [post]
func CreateInventoryHandler(w http.ResponseWriter, request *http.Request) {
	log.Printf("CreateProductsHandler: %+v", request)
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		log.Printf("/inventory error parsing request body: %+v", request)
		w.WriteHeader(http.StatusOK)
		return
	}
	bodyStr := string(body)

	var inventory products.Inventory
	if err := json.Unmarshal(body, &inventory); err != nil {
		errStr := fmt.Sprintf("Unable to unmarshal the inventoy JSON: %s Error: %+v", bodyStr, err)
		log.Printf(errStr)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(map[string]string{"error": errStr})
		return
	}
	log.Printf("Inventory to be created: %+v", inventory)

	if err := inventory_store.LoadStockItems(&inventory); err != nil {
		errStr := fmt.Sprintf("Unable to load the inventory: %+v Error: %+v", inventory, err)
		log.Printf(errStr)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(map[string]string{"error": errStr})
		return
	}

	log.Printf("Updated inventory: %+v", inventory)
	w.WriteHeader(http.StatusCreated)
}

// CreateProductsHandler godoc
// @Summary Create products
// @Description create products given in the JSON body
// @Accept json
// @Produce json
// @Param products body products.Products true "Products to add"
// @Success 200 {object} string
// @Failure 500 {object} string "database error creating products"
// @Router /products/ [post]
func CreateProductsHandler(w http.ResponseWriter, request *http.Request) {
	log.Printf("CreateProductsHandler: %+v", request)
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		log.Printf("/products error parsing request body: %+v", request)
		w.WriteHeader(http.StatusOK)
		return
	}
	bodyStr := string(body)

	var products products.LoadProducts
	if err := json.Unmarshal(body, &products); err != nil {
		errStr := fmt.Sprintf("Unable to unmarshal the products JSON: %s Error: %+v", bodyStr, err)
		log.Printf(errStr)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(map[string]string{"error": errStr})
		return
	}
	log.Printf("Proucts to be created: %+v", products)

	if err := inventory_store.LoadProducts(&products); err != nil {
		errStr := fmt.Sprintf("Unable to load the products: %+v Error: %+v", products, err)
		log.Printf(errStr)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(map[string]string{"error": errStr})
		return
	}

	log.Printf("Updated products: %+v", products)
	w.WriteHeader(http.StatusCreated)
}

// GetProductsHandler godoc
// @Summary Get products
// @Description get products
// @Produce json
// @Success 200 {object} products.Products
// @Failure 404 {object} string "no products found"
// @Failure 500 {object} string "database error finding products"
// @Router /products [get]
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
func GetProductsHandler(w http.ResponseWriter, request *http.Request) {
	log.Printf("GetProductsHandler: %+v", request)
	productsWithQuantity, err := inventory_store.InventoryStore.GetProductsWithQuantity()
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	if err != nil {
		errorStr := fmt.Sprintf("error no products with  found: %v", err)
		log.Println(errorStr)
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(map[string]string{"error": errorStr})
		return
	}
	log.Printf("GetProductsHandler: 200 OK: %+v", productsWithQuantity)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(productsWithQuantity)
}

// RemoveProductHandler godoc
// @Summary Remove product
// @Description remove product given in the JSON body
// @Accept json
// @Produce json
// @Param products body products.Products true "Product to remove"
// @Success 200 {object} string
// @Failure 500 {object} string "internal error deleting product"
// @Router /products/ [delete]
func RemoveProductHandler(w http.ResponseWriter, request *http.Request) {
	log.Printf("RemoveProductHandler: %+v", request)
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		log.Printf("/products error parsing request body: %+v", request)
		w.WriteHeader(http.StatusOK)
		return
	}
	bodyStr := string(body)

	var product products.Product
	if err := json.Unmarshal(body, &product); err != nil {
		errStr := fmt.Sprintf("Unable to unmarshal the product JSON: %s Error: %+v", bodyStr, err)
		log.Printf(errStr)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(map[string]string{"error": errStr})
		return
	}
	log.Printf("Proucts to be removed: %+v", product)

	if err = inventory_store.RemoveProduct(&product); err != nil {
		errStr := fmt.Sprintf("error removing the product JSON: %s Error: %+v", bodyStr, err)
		log.Printf(errStr)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(map[string]string{"error": errStr})
		return
	}
	w.WriteHeader(http.StatusOK)
}
