package inventoryapi

import (
	"bytes"
	"encoding/json"
	"gitlab.com/warehouse-inventory/internal/pkg/config"
	inventory_store "gitlab.com/warehouse-inventory/internal/pkg/inventory-store"
	"gitlab.com/warehouse-inventory/internal/pkg/products"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

var exampleInventoryJSON = `
{
  "inventory": [
    {
      "art_id": "1",
      "nam": "leg",
      "stock": "12"
    },
    {
      "art_id": "2",
      "name": "screw",
      "stock": "17"
    },
    {
      "art_id": "3",
      "name": "seat",
      "stock": "2"
    },
    {
      "art_id": "4",
      "name": "table top",
      "stock": "1"
    }
  ]
}`

var exampleProductsJSON = `
{
  "products": [
    {
      "name": "Dining Chair",
      "contain_articles": [
        {
          "art_id": "1",
          "amount_of": "4"
        },
        {
          "art_id": "2",
          "amount_of": "8"
        },
        {
          "art_id": "3",
          "amount_of": "1"
        }
      ]
    },
    {
      "name": "Dinning Table",
      "contain_articles": [
        {
          "art_id": "1",
          "amount_of": "4"
        },
        {
          "art_id": "2",
          "amount_of": "8"
        },
        {
          "art_id": "4",
          "amount_of": "1"
        }
      ]
    }
  ]
}`

// TestWarehouseHandlersFlow test create inventory, create products, get products quantities
func TestWarehouseHandlersFlow(t *testing.T) {
	if err := inventory_store.SetupInventoryRepository(&config.DatabaseConfiguration{StorageType: "memory"}); err != nil {
		t.Errorf("error setting up inventory repository")
	}
	reader := strings.NewReader(exampleInventoryJSON)
	req, err := http.NewRequest("POST", "/inventory", reader)
	if err != nil {
		t.Errorf("error creating inventory request: %v", err)
	}
	w := httptest.NewRecorder()
	CreateInventoryHandler(w, req)
	if w.Code != http.StatusCreated {
		t.Errorf("Could not create inventory page didn't return %v", http.StatusCreated)
	}

	readerProducts := strings.NewReader(exampleProductsJSON)
	reqProducts, err := http.NewRequest("POST", "/products", readerProducts)
	if err != nil {
		t.Errorf("error creating products request: %v", err)
	}

	wProducts := httptest.NewRecorder()
	CreateProductsHandler(wProducts, reqProducts)
	if wProducts.Code != http.StatusCreated {
		t.Errorf("Could not create products page didn't return %v", http.StatusCreated)
	}

	reqProductsQuantities, err := http.NewRequest("GET", "/products", nil)
	if err != nil {
		t.Errorf("error creating products quantities request: %v", err)
	}
	wProductsQuantities := httptest.NewRecorder()
	GetProductsHandler(wProductsQuantities, reqProductsQuantities)
	if wProductsQuantities.Code != http.StatusOK {
		t.Errorf("Could not get products quantities page didn't return %v", http.StatusOK)
	}

	body, err := ioutil.ReadAll(wProductsQuantities.Body)
	if err != nil {
		log.Printf("/products error parsing response body: %+v", wProductsQuantities)
		w.WriteHeader(http.StatusOK)
		return
	}
	bodyStr := string(body)

	if bodyStr == "" {
		t.Errorf("GET /produdts empty body")
	}

	var productsQuantities []products.ProductQuantity
	if err := json.Unmarshal(body, &productsQuantities); err != nil {
		t.Errorf("unable to unmarshal the products quantity entity JSON: %s Error: %+v", bodyStr, err)
	}

	if len(productsQuantities) == 0 {
		t.Errorf("empty /products reponse")
	} else if len(productsQuantities) != 2 {
		t.Errorf("incorrect number of products Quantities returned: %+v", productsQuantities)
	}

	var productToDelete *products.Product
	for _, productQuantity := range productsQuantities {
		if productQuantity.Product.Name == "Dining Chair" && productQuantity.Quantity != 2 {
			t.Errorf("incorrect Dining Chair quantity: %+v", productQuantity)
		} else if productQuantity.Product.Name == "Dining Chair" && productQuantity.Quantity == 2 {
			productToDelete = &productQuantity.Product
		}
		if productQuantity.Product.Name == "Dinning Table" && productQuantity.Quantity != 1 {
			t.Errorf("incorrect Dining Chair quantity: %+v", productQuantity)
		}
	}
	if productToDelete == nil {
		t.Errorf("no product to delete found")
	}
	bodyStrDelete, err := json.Marshal(productToDelete)
	if err != nil {
		t.Errorf("error marshalling product to delete: %+v %v", productToDelete, err)
	}

	readerProductsDelete := bytes.NewReader(bodyStrDelete)
	reqProductsDelete, err := http.NewRequest("DELETE", "/products", readerProductsDelete)
	if err != nil {
		t.Errorf("error deleting products request: %v", err)
	}

	wProductsDelete := httptest.NewRecorder()
	RemoveProductHandler(wProductsDelete, reqProductsDelete)
	if wProductsDelete.Code != http.StatusOK {
		t.Errorf("Could not delete products page didn't return %v", http.StatusOK)
	}
}
