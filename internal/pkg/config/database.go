package config

import (
	"fmt"
	"log"
)

// DatabaseConfiguration general database config including connection pool settings
type DatabaseConfiguration struct {
	Username, Password, Host, Database, StorageType  string
	Port, MaxIdleConnections, MaxOpenConnections     int
    ConnectionMaxLifetimeSeconds                     int64
}

func (d *DatabaseConfiguration) String() string {
	return fmt.Sprintf(`DatabaseConfiguration {Username: %s, Password: %s, Host: %s, Database: %s, Port: %d,
                       StorageType: %s MaxLifetimeSeconds: %d, maxIdelConnections: %d, maxOpenConnections: %d}`,
		d.Username, d.Password, d.Host, d.Database, d.Port, d.StorageType,
		d.ConnectionMaxLifetimeSeconds, d.MaxIdleConnections, d.MaxOpenConnections)
}

// LoadDatabaseConfig loads the MySqlConfig config from a file if specified, otherwise from the environment
func LoadDatabaseConfig() (*DatabaseConfiguration, error) {
	// TODO read from config file using a library like viper
	databaseConfig := DatabaseConfiguration{
		StorageType:                  "memory", // TODO update from config when adding SQL database implementation
	}
	log.Printf("Loaded DatabaseConfig updated: %+v", databaseConfig)

	if databaseConfig.MaxIdleConnections > databaseConfig.MaxOpenConnections {
		return nil, fmt.Errorf("error max idle connections: %d greater than max open connection: %d",
			databaseConfig.MaxIdleConnections, databaseConfig.MaxOpenConnections)
	}
	return &databaseConfig, nil
}
