package products

import "fmt"

/**
{
  "inventory": [
    {
      "art_id": "1",
      "name": "leg",
      "stock": "12"
    },
    {
      "art_id": "2",
      "name": "screw",
      "stock": "17"
    },
    {
      "art_id": "3",
      "name": "seat",
      "stock": "2"
    },
    {
      "art_id": "4",
      "name": "table top",
      "stock": "1"
    }
  ]
}
 */

// StockItem inside the Inventory with article ID name and stock
type StockItem struct {
	ArticleID string `json:"art_id"`
	Name      string `json:"name"`
	Stock     string `json:"stock"`
	StockInt  int
}

func (s StockItem) String() string {
	return fmt.Sprintf("StockItem: ArticleID: %s Name: %s Stock: %s StockInt: %d", s.ArticleID, s.Name, s.Stock, s.StockInt)
}

// Inventory containing many StockItems
type Inventory struct {
	StockItems []StockItem `json:"inventory"`
}

func (i Inventory) String() string {
	return fmt.Sprintf("Inventory: %+v", i.StockItems)
}
