package products

import "fmt"

/**
Example JSON:
{
  "products": [
    {
      "name": "Dining Chair",
      "contain_articles": [
        {
          "art_id": "1",
          "amount_of": "4"
        },
        {
          "art_id": "2",
          "amount_of": "8"
        },
        {
          "art_id": "3",
          "amount_of": "1"
        }
      ]
    },
    {
      "name": "Dinning Table",
      "contain_articles": [
        {
          "art_id": "1",
          "amount_of": "4"
        },
        {
          "art_id": "2",
          "amount_of": "8"
        },
        {
          "art_id": "4",
          "amount_of": "1"
        }
      ]
    }
  ]
}
*/

// Article the Id ad Amount of a particular article
type Article struct {
	ArticleID string `json:"art_id"`
	AmountOf  string `json:"amount_of"`
}

func (a Article) String() string {
	return fmt.Sprintf("Article: ArticleID: %s AmountOf: %s", a.ArticleID, a.AmountOf)
}

// LoadProducts used to load products into the Inventory
type LoadProducts struct {
	Products []Product `json:"products"`
}

// ContainArticles the articles within a product
type ContainArticles struct {
	ArtID       string `json:"art_id"`
	ArtIDInt    int
	AmountOf    string `json:"amount_of"`
	AmountOfInt int
}

// Product name, UUID and an array of articles
type Product struct {
	Name            string            `json:"name"`
	UUID            string
	ContainArticles []ContainArticles `json:"contain_articles"`
}

func (p Product) String() string {
	return fmt.Sprintf("Products: name: %s ContainArticles: %+v", p.Name, p.ContainArticles)
}

// ProductQuantity a product and the quantity available
type ProductQuantity struct {
	Product  Product `json:"product"`
	Quantity int  `json:"quantity"`
}

func (pq ProductQuantity) String() string {
	return fmt.Sprintf("ProductQuantity: quantity: %d %+v", pq.Quantity, pq.Product)
}
