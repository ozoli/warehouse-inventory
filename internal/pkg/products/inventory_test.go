package products

import (
	"encoding/json"
	"testing"
)

var exampleInventoryJSON = `
{
  "inventory": [
    {
      "art_id": "1",
      "name": "leg",
      "stock": "12"
    },
    {
      "art_id": "2",
      "name": "screw",
      "stock": "17"
    },
    {
      "art_id": "3",
      "name": "seat",
      "stock": "2"
    },
    {
      "art_id": "4",
      "name": "table top",
      "stock": "1"
    }
  ]
}`

func TestInventoryMarshalling(t *testing.T) {
	var inventory Inventory
	if err := json.Unmarshal([]byte(exampleInventoryJSON), &inventory); err != nil {
		t.Errorf("error parsing JSON: %s %v", exampleInventoryJSON, err)
	}
	if len(inventory.StockItems) != 4 {
		t.Errorf("error expected 4 Stock Items: %s %+v", exampleInventoryJSON, inventory)
	}
	if inventory.StockItems[1].Stock != "17" {
		t.Errorf("error expected 17 items available in StockItem 1: %s %+v", exampleInventoryJSON, inventory)
	}
}
