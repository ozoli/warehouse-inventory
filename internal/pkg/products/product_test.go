package products

import (
	"encoding/json"
	"log"
	"testing"
)

var exampleProductsJSON = `
{
  "products": [
    {
      "name": "Dining Chair",
      "contain_articles": [
        {
          "art_id": "1",
          "amount_of": "4"
        },
        {
          "art_id": "2",
          "amount_of": "8"
        },
        {
          "art_id": "3",
          "amount_of": "1"
        }
      ]
    },
    {
      "name": "Dinning Table",
      "contain_articles": [
        {
          "art_id": "1",
          "amount_of": "4"
        },
        {
          "art_id": "2",
          "amount_of": "8"
        },
        {
          "art_id": "4",
          "amount_of": "1"
        }
      ]
    }
  ]
}`

func TestProductsMarshalling(t *testing.T) {
	var products LoadProducts
	if err := json.Unmarshal([]byte(exampleProductsJSON), &products); err != nil {
		t.Errorf("error parsing JSON: %s %v", exampleProductsJSON, err)
	}
	log.Printf("Loaded Products: %+v", products)
	if products.Products[0].Name != "Dining Chair" {
		t.Errorf("error expected first Product to be Dining Chair: %s %+v", exampleProductsJSON, products)
	}
	if len(products.Products[0].ContainArticles) != 3 {
		t.Errorf("error expected first Product to be Dining Chair with 3 articles: %+v", products.Products[0])
	}
	if products.Products[0].ContainArticles[0].AmountOf != "4" {
		t.Errorf("error expected first Product to be Dining Chair with 1st articles amount 4: %+v", products.Products[0])
	}
	if products.Products[0].ContainArticles[1].AmountOf != "8" {
		t.Errorf("error expected first Product to be Dining Chair with 1st articles amount 8: %+v", products.Products[0])
	}
	if products.Products[1].Name != "Dinning Table" {
		t.Errorf("error expected first Product to be Dinning Table: %s %+v", exampleProductsJSON, products)
	}
	if len(products.Products[1].ContainArticles) != 3 {
		t.Errorf("error expected first Product to be Dinning Table with 3 articles: %s %+v", exampleProductsJSON, products)
	}
}
