package database

import (
	"database/sql"
	"fmt"
	"gitlab.com/warehouse-inventory/internal/pkg/config"
	"log"
	"time"
)

// SQLDb holds a connection to a MySQL instance and the prepared statements
// for executing against it.
type SQLDb struct {
	Conn     *sql.DB
	SQLStmts map[string]*sql.Stmt
}

// Close closes the database, freeing up any resources.
func (db *SQLDb) Close() error {
	var err error
	for _, stmt := range db.SQLStmts {
		if err = stmt.Close(); err != nil {
			log.Printf("Error: closing prepared statment: %+v error: %v", stmt, err)
		}
	}
	if err := db.Conn.Close(); err != nil {
		return fmt.Errorf("error closing database connection: %v", err)
	}
	return nil
}

// NewMySQLDb creates a new SQLDb backed by a given MySQL server.
func NewMySQLDb(config *SQLConfig, dbConfig *config.DatabaseConfiguration) (*SQLDb, error) {
	log.Printf("NewMySqlDb: config: %+v databaseConfig: %+v", config, dbConfig)
	conn, err := config.getConnection()
	if err != nil {
		return nil, fmt.Errorf("error creating SQL DB connection: %v", err)
	}
	if dbConfig.ConnectionMaxLifetimeSeconds > 0 {
		conn.SetConnMaxLifetime(time.Second * time.Duration(dbConfig.ConnectionMaxLifetimeSeconds))
	} else {
		log.Printf("WARN MySqlConfig ConnectionMaxLifetimeSeconds not set: %+v", dbConfig)
	}
	if dbConfig.MaxIdleConnections > 0 {
		conn.SetMaxIdleConns(dbConfig.MaxIdleConnections)
	} else {
		log.Printf("WARN MySqlConfig ConnectionIdleConnections not set: %+v", dbConfig)
	}
	if dbConfig.MaxOpenConnections > 0 {
		conn.SetMaxOpenConns(dbConfig.MaxOpenConnections)
	} else {
		log.Printf("WARN MySqlConfig MaxOpenConnections not set: %+v", dbConfig)
	}

	db := &SQLDb{
		Conn:     conn,
		SQLStmts: make(map[string]*sql.Stmt, 0),
	}
	for sqlCommandName, sqlCommand := range config.SQLCommands {
		// Prepared statements. The actual SQL queries are in the code near the relevant methods
		preparedCommand, err := conn.Prepare(sqlCommand)
		if err != nil {
			return nil, fmt.Errorf("mysql: prepare %s: error: %v", sqlCommand, err)
		}
		db.SQLStmts[sqlCommandName] = preparedCommand
	}
	return db, nil
}
