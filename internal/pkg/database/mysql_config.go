package database

import (
	"database/sql"
	"fmt"
	"log"
)

// CreateTableStatement table name and the SQL need to create it
type CreateTableStatement struct {
	TableName string
	SQL       []string
}

func (c CreateTableStatement) String() string {
	return fmt.Sprintf("CreateTableStatement: tableName: %s sql: %v", c.TableName, c.SQL)
}

// SQLConfig including table SQLs and connection pooling settings
type SQLConfig struct {
	// Name of the database to use
	Database string

	DriverName string  // eg. mysql or posgresql
	Username, Password string

	// Host of the SQL instance.
	// If set, UnixSocket should be unset.
	Host string

	// Port of the SQL instance.
	// If set, UnixSocket should be unset.
	Port int

	// UnixSocket is the filepath to a unix socket.
	// If set, Host and Port should be unset.
	UnixSocket string

	// Create table statements
	CreateTableStatements []CreateTableStatement

	// map of sql name (eg insert, findPeopleByName) and actual MySQL SQL command
	SQLCommands map[string]string

	// SQL Connection settings, MaxIdle <= MaxOpen
	ConnectionMaxLifetimeSeconds, ConnectionMaxIdleConnections, ConnectionMaxOpenConnections int
}

func (config SQLConfig) String() string {
	return fmt.Sprintf(`SQLConfig: db: %s username: %s passwd: %s driverName: %s connectionMaxLifetimeSeconds: %d 
                       connectionMaxIdleConnections: %d connectionMaxOpenConnections: %d CreateTableStatements: %+v SQLCommands: %v`,
                       config.Database, config.Username, config.Password, config.DriverName, config.ConnectionMaxLifetimeSeconds,
                       config.ConnectionMaxIdleConnections, config.ConnectionMaxOpenConnections, config.CreateTableStatements, config.SQLCommands)
}

// dataStoreName returns a connection string suitable for sql.Open.
func (config SQLConfig) dataStoreName(databaseName string) string {
	var cred string
	// [username[:password]@]
	if config.Username != "" {
		cred = config.Username
		if config.Password != "" {
			cred = cred + ":" + config.Password
		}
		cred = cred + "@"
	}

	if config.UnixSocket != "" {
		return fmt.Sprintf("%sunix(%s)/%s", cred, config.UnixSocket, databaseName)
	}
	return fmt.Sprintf("%stcp([%s]:%d)/%s", cred, config.Host, config.Port, databaseName)
}

func (config SQLConfig) getConnection() (*sql.DB, error) {
	conn, err := sql.Open(config.DriverName, config.dataStoreName(config.Database))
	if err != nil {
		return nil, fmt.Errorf("mysql: could not get a connection: %v", err)
	}
	if err := conn.Ping(); err != nil {
		if err = conn.Close(); err != nil {
			log.Printf("Error closing SQL connection after ping failure: %v", err)
		}
		return nil, fmt.Errorf("mysql: could not establish a good connection: %v", err)
	}
	return conn, nil
}
