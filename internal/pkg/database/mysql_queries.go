package database

import (
	"database/sql"
	"fmt"
	"log"
)

// RowScanner is implemented by sql.Row and sql.Rows
type RowScanner interface {
	Scan(dest ...interface{}) error
}

// ExecAffectingOneRow executes a given statement, expecting one row to be affected.
func ExecAffectingOneRow(stmt *sql.Stmt, args ...interface{}) (sql.Result, error) {
	r, err := stmt.Exec(args...)
	if err != nil {
		return r, fmt.Errorf("mysql: could not execute statement: %v", err)
	}
	rowsAffected, err := r.RowsAffected()
	if err != nil {
		return r, fmt.Errorf("mysql: could not get rows affected: %v", err)
	} else if rowsAffected != 1 {
		return r, fmt.Errorf("mysql: expected 1 row affected, got %d", rowsAffected)
	}
	return r, nil
}

// ExecAffectingSomeRows executes a given statement, expecting maybe some rows to be affected.
func ExecAffectingSomeRows(stmt *sql.Stmt, args ...interface{}) (sql.Result, error) {
	r, err := stmt.Exec(args...)
	if err != nil {
		return r, fmt.Errorf("mysql: could not execute statement: %v", err)
	}
	rowsAffected, err := r.RowsAffected()
	if err != nil {
		return r, fmt.Errorf("mysql: could not get rows affected: %v", err)
	}
	log.Printf("mysql: %d rows affected", rowsAffected)
	return r, nil
}
